Hello. This is a tool for retiming animations in Blender 2.92 and up. It is licensed under the GPLv3.

If you have income, please purchase the addon on Blender Market, so that I will continue maintaining it for new versions of Blender.
If you are poor, please download and use it freely.

Be well.
Sincerely,
The Rabbit Man


#### New in 1.1:
- Added: Blender 4.X support (Thanks to Pluglug for their contributions!)
- Added: Shape Key support.
- Added: Grease Pencil support (As grease pencil frames are always integers, Partial Keyframe support is disabled when the selection includes them).
- Added: Panel for filtering which types of keyframes (Object, Grease Pencil, Shape Key) will be affected by retiming.
- Added: Panel for easily toggling of addon preferences. (Useful in conjunction with filtering)
- Added: Error message display when keyframes will be overwritten.
- Added: Retime Markers option to the Timeline editor's Marker menu
- Changed: Header now shows the retiming duration before and after, along with the percentage change.
- Changed: Footer now shows operator modifier keys, as similar to builtin blender operators as possible with the current Blender API (no icons, sadly).
- Changed: Operator now skips processing objects that have no animation data, which should improve performance slightly.
- Changed: Error Message displays when no keyframes are selected, now in all situations.
