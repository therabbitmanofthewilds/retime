# This file is part of Retime.

# Retime is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Retime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Retime.  If not, see <https://www.gnu.org/licenses/>.

import bpy
BEFORE_3_0 = bpy.app.version < (3, 0, 0)
BEFORE_4_3 = bpy.app.version < (4, 3, 0)

bl_info = {
    "name": "Retime",
    "author": "The Rabbit Man of the Wilds, with contributions by the most honorable Pluglug",
    "version": (1, 1, 0),
    "blender": (2, 92, 0),
    "location": "Dope Sheet, Graph Editor",
    "description": "",
    "warning": "",
    "category": "Animation"
}

if "retime" not in locals():
    from . import retime
else:
    import importlib
    retime = importlib.reload(retime)

classes = (
    retime.RetimePreferences,
    retime.RetimeKeyframes,
    retime.RetimeMarkers,
    retime.RetimeFilter,
    retime.RetimeDopeSheetFilterPanel,
    retime.RetimeGraphFilterPanel,
    retime.RetimeDopeSheetPreferencesPanel,
    retime.RetimeGraphPreferencesPanel
)
register_classes, unregister_classes = bpy.utils.register_classes_factory(classes)

addon_keymaps = []


def register():
    bpy.types.DOPESHEET_MT_key.append(retime.retime_keyframes_menu)
    bpy.types.DOPESHEET_MT_marker.append(retime.retime_markers_menu)
    if BEFORE_3_0:
        bpy.types.DOPESHEET_MT_gpencil_frame.append(retime.retime_keyframes_menu)
    if BEFORE_4_3:
        bpy.types.DOPESHEET_MT_gpencil_key.append(retime.retime_keyframes_menu)
    bpy.types.GRAPH_MT_key.append(retime.retime_keyframes_menu)
    bpy.types.GRAPH_MT_marker.append(retime.retime_markers_menu)
    bpy.types.TIME_MT_marker.append(retime.retime_markers_menu)

    register_classes()

    bpy.types.Scene.retime_filter = bpy.props.PointerProperty(type=retime.RetimeFilter)

    wm = bpy.context.window_manager
    if wm.keyconfigs.addon is None:
        # This happens when Blender is running in the background.
        return

    km = wm.keyconfigs.addon.keymaps.new(name='Dopesheet', space_type='EMPTY')
    kmi = km.keymap_items.new('scene.retime_keyframes', 'LEFTMOUSE', 'CLICK', any=False, shift=False, ctrl=True, alt=False, oskey=False, key_modifier='NONE')
    kmi.active = True
    addon_keymaps.append(km)


def unregister():
    wm = bpy.context.window_manager
    if wm.keyconfigs.addon is None:
        # This happens when Blender is running in the background.
        return

    for km in addon_keymaps:
        wm.keyconfigs.addon.keymaps.remove(km)
    del addon_keymaps[:]

    del bpy.types.Scene.retime_filter

    unregister_classes()

    bpy.types.TIME_MT_marker.remove(retime.retime_markers_menu)
    bpy.types.GRAPH_MT_marker.remove(retime.retime_markers_menu)
    bpy.types.GRAPH_MT_key.remove(retime.retime_keyframes_menu)
    if BEFORE_4_3:
        bpy.types.DOPESHEET_MT_gpencil_key.remove(retime.retime_keyframes_menu)
    if BEFORE_3_0:
        bpy.types.DOPESHEET_MT_gpencil_frame.remove(retime.retime_keyframes_menu)
    bpy.types.DOPESHEET_MT_marker.remove(retime.retime_markers_menu)
    bpy.types.DOPESHEET_MT_key.remove(retime.retime_keyframes_menu)
