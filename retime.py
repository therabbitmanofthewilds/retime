# This file is part of Retime.

# Retime is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Retime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Retime.  If not, see <https://www.gnu.org/licenses/>.

import bpy
from bpy.props import (BoolProperty)
from bpy.types import (Operator, Panel, AddonPreferences, PropertyGroup)

import gpu
from gpu_extras.batch import batch_for_shader

SINCE_4_0 = bpy.app.version >= (4, 0, 0)

if SINCE_4_0 == False:
    import bgl


GLOBAL_MAX_FRAME = 999999
GLOBAL_MIN_FRAME = -999999


def get_anim_snap_mode(context):
    if SINCE_4_0:
        return context.tool_settings.snap_anim_element
    else:
        return context.space_data.auto_snap


def begin_alpha_blending():
    if SINCE_4_0:
        gpu.state.blend_set('ALPHA')
    else:
        bgl.glEnable(bgl.GL_BLEND)
        bgl.glBlendFunc(bgl.GL_SRC_ALPHA, bgl.GL_ONE_MINUS_SRC_ALPHA)


def end_alpha_blending():
    if SINCE_4_0:
        gpu.state.blend_set('NONE')
    else:
        bgl.glDisable(bgl.GL_BLEND)


class RetimePreferences(AddonPreferences):
    bl_idname = __package__

    adjust_playback_duration: BoolProperty(
        name = "Adjust duration of animation",
        default = True
    )

    adjust_playhead_position: BoolProperty(
        name = "Adjust position of playhead",
        default = True
    )

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        col = row.column()
        col.prop(self, "adjust_playback_duration")
        col.prop(self, "adjust_playhead_position")


class RetimedKeyframeCoordinateAndHandles:
    def __init__(self, co_x, handle_left_co, handle_right_co):
        self.co_x = co_x
        self.old_co_x = co_x
        self.handle_left_co = handle_left_co
        self.handle_right_co = handle_right_co


class RetimedPoseMarkers:
    def __init__(self, obj_index):
        self.obj_index = obj_index
        self.data = []


class RetimedMarkerFrameAndIndex:
    def __init__(self, frame, index):
        self.frame = frame
        self.index = index


indices = ((0, 1, 2), (2, 1, 3))
shader = gpu.shader.from_builtin('UNIFORM_COLOR' if SINCE_4_0 else '2D_UNIFORM_COLOR')
bar_diameter = 0.5


def draw_quad_px(context, start, end, color):
    range_batch = batch_for_shader(
        shader,
        'TRIS',
        {"pos": [
            (start, 0),
            (end, 0),
            (start, context.region.height),
            (end, context.region.height)
        ]},
        indices=indices
    )
    shader.bind()
    shader.uniform_float("color", (color[0], color[1], color[2], 0.5))
    range_batch.draw(shader)


def draw_bar_px(context, pos, color):
    draw_quad_px(context, pos - bar_diameter, pos + bar_diameter, color)


def draw_callback_px(self, context, color, bar_color):
    view_to_region = context.region.view2d.view_to_region

    start = view_to_region(self.start_frame, 0, clip=False)[0]
    initial_end = view_to_region(self.start_frame + round(self.initial_timing), 0, clip=False)[0]
    desired_end = view_to_region(self.start_frame + round(self.desired_timing), 0, clip=False)[0]

    begin_alpha_blending()

    draw_quad_px(context, start, initial_end, color)
    draw_quad_px(context, start, desired_end, color)

    draw_bar_px(context, start, bar_color)
    draw_bar_px(context, initial_end, bar_color)
    draw_bar_px(context, desired_end, bar_color)

    end_alpha_blending()


def linear_interpolation(value, start_frame, end_frame, desired_timing):
    return (((value - start_frame) / (end_frame - start_frame)) * desired_timing) + start_frame


def apply_retiming(self, context):
    preferences = context.preferences.addons[__package__].preferences
    auto_snap = get_anim_snap_mode(context)
    offset = self.desired_timing - self.current_timing
    end_frame = self.start_frame + self.current_timing
    is_retiming_filtered = not(context.scene.retime_filter.retime_objects and context.scene.retime_filter.retime_grease_pencils and context.scene.retime_filter.retime_shape_keys)

    if preferences.adjust_playback_duration:
        if context.scene.frame_end > self.start_frame and context.scene.frame_end <= end_frame:
            context.scene.frame_end = round(
                linear_interpolation(
                    float(context.scene.frame_end),
                    self.start_frame,
                    end_frame,
                    self.desired_timing
                )
            )
        elif context.scene.frame_end > end_frame:
            context.scene.frame_end += offset


    if preferences.adjust_playhead_position:
        if context.scene.frame_current > self.start_frame and context.scene.frame_current <= end_frame:
            context.scene.frame_current = round(
                linear_interpolation(
                    float(context.scene.frame_current),
                    self.start_frame,
                    end_frame,
                    self.desired_timing
                )
            )
        elif context.scene.frame_current > end_frame:
            context.scene.frame_current += offset

    if context.scene.retime_filter.retime_objects:
        for ro_i in range(len(self.retimed_object_indices)):
            so_i = self.retimed_object_indices[ro_i]
            nla_tweak_strip_time_to_scene = self.affected_objects[so_i].animation_data.nla_tweak_strip_time_to_scene
            action = self.affected_objects[so_i].animation_data.action

            for rf_i in range(len(self.retimed_fcurve_indices[ro_i])):
                f_i = self.retimed_fcurve_indices[ro_i][rf_i]
                k_i = self.retimed_keyframes_start_indices[ro_i][rf_i]
                for rk_i in range(len(self.retimed_keyframes[ro_i][rf_i])):
                    #TODO: I think writing to co_x here is unnecessary, just directly jump to the branch below
                    self.retimed_keyframes[ro_i][rf_i][rk_i].co_x = linear_interpolation(self.retimed_keyframes[ro_i][rf_i][rk_i].co_x, self.start_frame, end_frame, self.desired_timing)
                    self.retimed_keyframes[ro_i][rf_i][rk_i].old_co_x = self.retimed_keyframes[ro_i][rf_i][rk_i].co_x
                    self.retimed_keyframes[ro_i][rf_i][rk_i].handle_left_co[0] = linear_interpolation(self.retimed_keyframes[ro_i][rf_i][rk_i].handle_left_co[0], self.start_frame, end_frame, self.desired_timing)
                    self.retimed_keyframes[ro_i][rf_i][rk_i].handle_right_co[0] = linear_interpolation(self.retimed_keyframes[ro_i][rf_i][rk_i].handle_right_co[0], self.start_frame, end_frame, self.desired_timing)

                    if (auto_snap == 'STEP' and self.is_ctrl_down == False) or self.gpencil_keyframes_selected:
                        action.fcurves[f_i].keyframe_points[k_i].co.x = nla_tweak_strip_time_to_scene(round(self.retimed_keyframes[ro_i][rf_i][rk_i].co_x), invert=True)
                    else:
                        action.fcurves[f_i].keyframe_points[k_i].co.x = nla_tweak_strip_time_to_scene(self.retimed_keyframes[ro_i][rf_i][rk_i].co_x, invert=True)

                    #TODO: I hope this catches everything, see the snap behavior described below
                    if rk_i > 0 and self.is_ctrl_down == False and round(self.retimed_keyframes[ro_i][rf_i][rk_i].co_x) == round(self.retimed_keyframes[ro_i][rf_i][rk_i-1].co_x):
                        self.overwrote_keyframes = True
                    k_i += 1

            for prf_i in range(len(self.post_retimed_fcurve_indices[ro_i])):
                f_i = self.post_retimed_fcurve_indices[ro_i][prf_i]
                for prk_i in range(self.post_retimed_keyframes_start_indices[ro_i][prf_i], len(action.fcurves[f_i].keyframe_points)):
                    action.fcurves[f_i].keyframe_points[prk_i].co.x += offset
                    action.fcurves[f_i].keyframe_points[prk_i].handle_left[0] += offset
                    action.fcurves[f_i].keyframe_points[prk_i].handle_right[0] += offset

    if context.scene.retime_filter.retime_shape_keys:
        for rsko_i in range(len(self.retimed_shapekey_object_indices)):
            ssko_i = self.retimed_shapekey_object_indices[rsko_i]
            nla_tweak_strip_time_to_scene = self.affected_objects[ssko_i].data.shape_keys.animation_data.nla_tweak_strip_time_to_scene
            action = self.affected_objects[ssko_i].data.shape_keys.animation_data.action

            for rskf_i in range(len(self.retimed_shapekey_fcurve_indices[rsko_i])):
                skf_i = self.retimed_shapekey_fcurve_indices[rsko_i][rskf_i]
                skk_i = self.retimed_shapekey_keyframes_start_indices[rsko_i][rskf_i]
                for rskk_i in range(len(self.retimed_shapekey_keyframes[rsko_i][rskf_i])):
                    #TODO: I think writing to co_x here is unnecessary, just directly jump to the branch below
                    self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].co_x = linear_interpolation(self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].co_x, self.start_frame, end_frame, self.desired_timing)
                    self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].old_co_x = self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].co_x
                    self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].handle_left_co[0] = linear_interpolation(self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].handle_left_co[0], self.start_frame, end_frame, self.desired_timing)
                    self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].handle_right_co[0] = linear_interpolation(self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].handle_right_co[0], self.start_frame, end_frame, self.desired_timing)

                    if (auto_snap == 'STEP' and self.is_ctrl_down == False) or self.gpencil_keyframes_selected:
                        action.fcurves[skf_i].keyframe_points[skk_i].co.x = nla_tweak_strip_time_to_scene(round(self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].co_x), invert=True)
                    else:
                        action.fcurves[skf_i].keyframe_points[skk_i].co.x = nla_tweak_strip_time_to_scene(self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].co_x, invert=True)

                    #TODO: I hope this catches everything, see the snap behavior described below
                    if rskk_i > 0 and self.is_ctrl_down == False and round(self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i].co_x) == round(self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i-1].co_x):
                        self.overwrote_keyframes = True
                    skk_i += 1

            for prskf_i in range(len(self.post_retimed_shapekey_fcurve_indices[rsko_i])):
                skf_i = self.post_retimed_shapekey_fcurve_indices[rsko_i][prskf_i]
                for prskk_i in range(self.post_retimed_shapekey_keyframes_start_indices[rsko_i][prskf_i], len(action.fcurves[skf_i].keyframe_points)):
                    action.fcurves[skf_i].keyframe_points[prskk_i].co.x += offset
                    action.fcurves[skf_i].keyframe_points[prskk_i].handle_left[0] += offset
                    action.fcurves[skf_i].keyframe_points[prskk_i].handle_right[0] += offset

    if context.scene.retime_filter.retime_grease_pencils:
        for rgo_i in range(len(self.retimed_gpencil_object_indices)):
            sgo_i = self.retimed_gpencil_object_indices[rgo_i]
            layers = self.affected_objects[sgo_i].data.layers

            for rgl_i in range(len(self.retimed_gpencil_layer_indices[rgo_i])):
                gl_i = self.retimed_gpencil_layer_indices[rgo_i][rgl_i]
                gk_i = self.retimed_gpencil_keyframes_start_indices[rgo_i][rgl_i]
                for rgk_i in range(len(self.retimed_gpencil_keyframes[rgo_i][rgl_i])):
                    self.retimed_gpencil_keyframes[rgo_i][rgl_i][rgk_i] = round(linear_interpolation(self.retimed_gpencil_keyframes[rgo_i][rgl_i][rgk_i], self.start_frame, end_frame, self.desired_timing))
                    layers[gl_i].frames[gk_i].frame_number = int(self.retimed_gpencil_keyframes[rgo_i][rgl_i][rgk_i])

                    if rgk_i > 0 and int(self.retimed_gpencil_keyframes[rgo_i][rgl_i][rgk_i]) == int(self.retimed_gpencil_keyframes[rgo_i][rgl_i][rgk_i-1]):
                        self.overwrote_keyframes = True
                    gk_i += 1

            for prgl_i in range(len(self.post_retimed_gpencil_layer_indices[rgo_i])):
                gl_i = self.post_retimed_gpencil_layer_indices[rgo_i][prgl_i]
                for prgk_i in range(self.post_retimed_gpencil_keyframes_start_indices[rgo_i][prgl_i], len(layers[gl_i].frames)):
                    layers[gl_i].frames[prgk_i].frame_number += offset

    # NOTE: The blender team is intent on giving me grey hair before my time.
    # Not only is there a snap function per editor, and *not only* do
    # they give subtly different behavior, but neither give the same behavior
    # as *the built in snapping* of the scale op.
    # Until someone in blender.chat responds to my queries on how to do this better,
    # this is as good as it gets.
    # I am sorry my friend. I have tried my best.
    if auto_snap == 'FRAME':
        if self.is_ctrl_down == False:
            if context.space_data.type == 'DOPESHEET_EDITOR':
                bpy.ops.action.snap(type='NEAREST_FRAME')
            elif context.space_data.type == 'GRAPH_EDITOR':
                bpy.ops.graph.snap(type='NEAREST_FRAME')
    elif auto_snap == 'NONE':
        if self.is_ctrl_down:
            if context.space_data.type == 'DOPESHEET_EDITOR':
                bpy.ops.action.snap(type='NEAREST_FRAME')
            elif context.space_data.type == 'GRAPH_EDITOR':
                bpy.ops.graph.snap(type='NEAREST_FRAME')

    # NOTE: This fixes the fcurve handle misalignment from the snap operator. I wish there was a way to do this without another loop, but it seems not.
    if self.overwrote_keyframes == False:
        if context.scene.retime_filter.retime_objects:
            for ro_i in range(len(self.retimed_object_indices)):
                so_i = self.retimed_object_indices[ro_i]
                nla_tweak_strip_time_to_scene = self.affected_objects[so_i].animation_data.nla_tweak_strip_time_to_scene
                action = self.affected_objects[so_i].animation_data.action
                for rf_i in range(len(self.retimed_fcurve_indices[ro_i])):
                    f_i = self.retimed_fcurve_indices[ro_i][rf_i]
                    k_i = self.retimed_keyframes_start_indices[ro_i][rf_i]
                    for rk_i in range(len(self.retimed_keyframes[ro_i][rf_i])):
                        fk = action.fcurves[f_i].keyframe_points[k_i]
                        rk = self.retimed_keyframes[ro_i][rf_i][rk_i]
                        diff = fk.co.x - nla_tweak_strip_time_to_scene(rk.old_co_x, invert=False)
                        fk.handle_left[0] = nla_tweak_strip_time_to_scene(rk.handle_left_co[0], invert=True) + diff
                        fk.handle_right[0] = nla_tweak_strip_time_to_scene(rk.handle_right_co[0], invert=True) + diff
                        fk.handle_left[1] = rk.handle_left_co[1]
                        fk.handle_right[1] = rk.handle_right_co[1]
                        k_i += 1
        if context.scene.retime_filter.retime_shape_keys:
            for rsko_i in range(len(self.retimed_shapekey_object_indices)):
                ssko_i = self.retimed_shapekey_object_indices[rsko_i]
                nla_tweak_strip_time_to_scene = self.affected_objects[ssko_i].data.shape_keys.animation_data.nla_tweak_strip_time_to_scene
                action = self.affected_objects[ssko_i].data.shape_keys.animation_data.action
                for rskf_i in range(len(self.retimed_shapekey_fcurve_indices[rsko_i])):
                    skf_i = self.retimed_shapekey_fcurve_indices[rsko_i][rskf_i]
                    skk_i = self.retimed_shapekey_keyframes_start_indices[rsko_i][rskf_i]
                    for rskk_i in range(len(self.retimed_shapekey_keyframes[rsko_i][rskf_i])):
                        fk = action.fcurves[skf_i].keyframe_points[skk_i]
                        rk = self.retimed_shapekey_keyframes[rsko_i][rskf_i][rskk_i]
                        diff = fk.co.x - nla_tweak_strip_time_to_scene(rk.old_co_x, invert=False)
                        fk.handle_left[0] = nla_tweak_strip_time_to_scene(rk.handle_left_co[0], invert=True) + diff
                        fk.handle_right[0] = nla_tweak_strip_time_to_scene(rk.handle_right_co[0], invert=True) + diff
                        fk.handle_left[1] = rk.handle_left_co[1]
                        fk.handle_right[1] = rk.handle_right_co[1]
                        skk_i += 1

    if context.scene.retime_filter.retime_objects:
        for so_i in self.retimed_object_indices:
            for fcurve in self.affected_objects[so_i].animation_data.action.fcurves:
                fcurve.update()

    if context.scene.retime_filter.retime_shape_keys:
        for ssko_i in self.retimed_shapekey_object_indices:
            for fcurve in self.affected_objects[ssko_i].data.shape_keys.animation_data.action.fcurves:
                fcurve.update()

    # Markers
    for sm_i in range(len(self.sorted_marker_frames_and_indices)):
        # Retimed markers start
        m_i = self.sorted_marker_frames_and_indices[sm_i].index
        if self.sorted_marker_frames_and_indices[sm_i].frame > self.start_frame and self.sorted_marker_frames_and_indices[sm_i].frame <= end_frame:
            self.sorted_marker_frames_and_indices[sm_i].frame = linear_interpolation(
                self.sorted_marker_frames_and_indices[sm_i].frame,
                self.start_frame,
                end_frame,
                self.desired_timing
            )
            context.scene.timeline_markers[m_i].frame = int(round(self.sorted_marker_frames_and_indices[sm_i].frame))
        # Post Retimed markers start
        elif self.sorted_marker_frames_and_indices[sm_i].frame > end_frame:
            self.sorted_marker_frames_and_indices[sm_i].frame += offset
            context.scene.timeline_markers[m_i].frame = int(round(self.sorted_marker_frames_and_indices[sm_i].frame))

    # Pose Markers
    for pose_markers in self.sorted_pose_marker_frames_and_indices:
        action = self.affected_objects[pose_markers.obj_index].animation_data.action
        for pose_marker in pose_markers.data:
            if pose_marker.frame > self.start_frame and pose_marker.frame <= end_frame:
                pose_marker.frame = linear_interpolation(
                    pose_marker.frame,
                    self.start_frame,
                    end_frame,
                    self.desired_timing
                )
                action.pose_markers[pose_marker.index].frame = int(round(pose_marker.frame))
            elif pose_marker.frame > end_frame:
                pose_marker.frame += offset
                action.pose_markers[pose_marker.index].frame = int(round(pose_marker.frame))


# def custom_keyframes_overwritten_warning(self, context):
#     self.layout.label(text="Retiming overwrote keyframes")


def modal_common(self, context, event, name):
    #CTRL
    if (event.type == "LEFT_CTRL" or event.type == "RIGHT_CTRL") and event.is_repeat == False:
        if self.is_ctrl_down:
            self.is_ctrl_down = False
        else:
            self.is_ctrl_down = True
    #FINISH
    if event.type == 'LEFTMOUSE' or event.type == 'RET' or event.type == 'NUMPAD_ENTER':
        if self.desired_timing != self.initial_timing:
            apply_retiming(self, context)
        context.area.header_text_set(None)
        if context.space_data.type == 'DOPESHEET_EDITOR':
            bpy.types.SpaceDopeSheetEditor.draw_handler_remove(self.draw_handle, 'WINDOW')
        elif context.space_data.type == 'GRAPH_EDITOR':
            bpy.types.SpaceGraphEditor.draw_handler_remove(self.draw_handle, 'WINDOW')
        context.region.tag_redraw()
        if self.overwrote_keyframes:
            self.report({'ERROR'}, "Retiming overwrote keyframes")
            # context.window_manager.popup_menu(custom_keyframes_overwritten_warning, title="Warning", icon='ERROR')
        return {'FINISHED'}
    #CANCEL
    if event.type in {'RIGHTMOUSE', 'ESC'}:
        context.area.header_text_set(None)
        if context.space_data.type == 'DOPESHEET_EDITOR':
            bpy.types.SpaceDopeSheetEditor.draw_handler_remove(self.draw_handle, 'WINDOW')
        elif context.space_data.type == 'GRAPH_EDITOR':
            bpy.types.SpaceGraphEditor.draw_handler_remove(self.draw_handle, 'WINDOW')
        context.region.tag_redraw()
        return {'CANCELLED'}
    #KEYBOARD NUMBER INPUT
    if event.ascii.isnumeric() and event.is_repeat == False:
        if event.ascii == '0' and self.keyboard_cursor_position == 0:
            pass
        else:
            if self.is_keyboard_input == False:
                self.desired_timing = round(float(event.ascii))
                #print("str desired timing: {}".format(str(round(self.desired_timing))))
                self.keyboard_cursor_position = len(str(round(self.desired_timing)))
                #print("keyboard cursor position = {}".format(self.keyboard_cursor_position))
                self.is_keyboard_input = True
            else:
                #print("keyboard cursor position = {}".format(self.keyboard_cursor_position))
                s = str(round(self.desired_timing))
                unsanitized_desired_timing = round(float(s[0:self.keyboard_cursor_position] + event.ascii + s[self.keyboard_cursor_position:len(s)]))
                #HACK: I could be more scientific about this, but really, you have no use case for it, do you? Admit it. Look into my eyes.
                if unsanitized_desired_timing > GLOBAL_MAX_FRAME:
                    pass
                else:
                    self.desired_timing = unsanitized_desired_timing
                    self.keyboard_cursor_position += 1
                    #print("str desired timing: {}".format(str(round(self.desired_timing))))
    #BACKSPACE
    #TODO: backspace and delete share most of their code
    elif event.type == 'BACK_SPACE' and event.is_repeat == False:
        if self.is_backspace_down:
            self.is_backspace_down = False
        else:
            self.is_backspace_down = True

        if self.is_keyboard_input == True and self.is_backspace_down == True and self.keyboard_cursor_position != 0:
            self.keyboard_cursor_position -= 1

            s = str(round(self.desired_timing))
            s = s[0:self.keyboard_cursor_position] + s[self.keyboard_cursor_position+1:len(s)]
            s = s.lstrip('0')

            if s == "":
                self.desired_timing = self.initial_timing
                self.keyboard_cursor_position = len(str(round(self.desired_timing)))
                self.is_keyboard_input = False
            else:
                self.desired_timing = round(float(s))
    #DELETE
    #TODO: backspace and delete share most of their code
    elif event.type == 'DEL' and event.is_repeat == False:
        if self.is_delete_down:
            self.is_delete_down = False
        else:
            self.is_delete_down = True

        s = str(round(self.desired_timing))
        if self.is_keyboard_input == True and self.is_delete_down == True and self.keyboard_cursor_position != len(s):

            s = str(round(self.desired_timing))
            s = s[0:self.keyboard_cursor_position] + s[self.keyboard_cursor_position+1:len(s)]
            s = s.lstrip('0')

            if s == "":
                self.desired_timing = self.initial_timing
                self.keyboard_cursor_position = len(str(round(self.desired_timing)))
                self.is_keyboard_input = False
            else:
                self.desired_timing = round(float(s))
    #KEYBOARD ARROWS
    elif event.type == 'RIGHT_ARROW' and event.is_repeat == False:
        if self.is_right_arrow_down:
            self.is_right_arrow_down = False
        else:
            self.is_right_arrow_down = True

        if self.is_right_arrow_down == True:
            if self.is_keyboard_input == True:
                if self.keyboard_cursor_position != len(str(self.desired_timing)):
                    self.keyboard_cursor_position += 1
            else:
                #HACK: I could be more scientific about this, but really, you have no use case for it, do you? Admit it. Look into my eyes.
                if self.current_timing + 1 <= GLOBAL_MAX_FRAME:
                    self.desired_timing += 1

    elif event.type == 'LEFT_ARROW' and event.is_repeat == False:
        if self.is_left_arrow_down:
            self.is_left_arrow_down = False
        else:
            self.is_left_arrow_down = True

        if self.is_left_arrow_down == True:
            if self.is_keyboard_input == True:
                if self.keyboard_cursor_position != 0:
                    self.keyboard_cursor_position -= 1
            else:
                if self.current_timing - 1 >= 1:
                    self.desired_timing -= 1

    #MOUSE
    elif event.type == 'MOUSEMOVE' and self.is_keyboard_input == False:
        region_to_view = context.region.view2d.region_to_view
        mouse_x = event.mouse_region_x
        # NOTE: move by integers, but accumulate fractional mouse offset
        # this ties into respecting auto_snap
        float_offset = region_to_view(mouse_x, 0)[0] - region_to_view(self.mouse_prev_x, 0)[0]
        int_offset = int(float_offset)
        frac_offset = float_offset - int_offset

        int_residual_offset = int(self.residual_offset)
        frac_residual_offset = self.residual_offset - int_residual_offset

        offset = int_offset + int_residual_offset
        if self.desired_timing + offset < 1:
            offset = 1 - self.desired_timing

        self.mouse_prev_x = mouse_x
        self.residual_offset = frac_offset + frac_residual_offset
        self.desired_timing += offset

    auto_snap = get_anim_snap_mode(context)
    will_generate_partial_keyframes = False
    if auto_snap == 'FRAME':
        if self.is_ctrl_down:
            will_generate_partial_keyframes = True
    elif auto_snap == 'NONE':
        if self.is_ctrl_down == False:
            will_generate_partial_keyframes = True
    elif auto_snap == 'STEP':
        if self.is_ctrl_down:
            will_generate_partial_keyframes = True

    if self.gpencil_keyframes_selected:
        context.workspace.status_text_set(text="[Ctrl]: Make Partial Keyframes (Disabled due to Grease Pencil keyframes in selection)")
    else:
        context.workspace.status_text_set(text="[Ctrl]: Make Partial Keyframes ({})".format(will_generate_partial_keyframes))
    rounded_desired_timing = round(self.desired_timing)
    percentage = str(round((rounded_desired_timing/self.initial_timing)*100))
    if self.is_keyboard_input:
        s = str(rounded_desired_timing)
        context.area.header_text_set("Retiming range of {} from {} to [".format(name, self.initial_timing) + s[0:self.keyboard_cursor_position] + "|" + s[self.keyboard_cursor_position:len(s)] + "] ({}%)".format(percentage))
    else:
        context.area.header_text_set("Retiming range of {} from {} to ".format(name, self.initial_timing) + str(rounded_desired_timing) + " ({}%)".format(percentage))
    context.region.tag_redraw()

    return {'RUNNING_MODAL'}

def invoke_common(self, context, event, use_markers):
    if context == None or context.area == None:
        return {'CANCELLED'}

    self.affected_objects = []
    if context.space_data.type == 'DOPESHEET_EDITOR':
        if context.space_data.mode == 'DOPESHEET':
            self.affected_objects = context.selected_editable_objects
        elif context.space_data.mode == 'TIMELINE':
            self.affected_objects = context.selected_editable_objects
        elif context.space_data.mode == 'ACTION':
            self.affected_objects.append(context.active_object)
        elif context.space_data.mode == 'GPENCIL':
            self.affected_objects = context.selected_editable_objects
        elif context.space_data.mode == 'SHAPEKEY':
            self.affected_objects = context.selected_editable_objects
        elif context.space_data.mode == 'MASK':
            self.report({'ERROR'}, "Retiming masks is unimplemented")
            return {'CANCELLED'}
        else:
            return {'CANCELLED'}

    elif context.space_data.type == 'GRAPH_EDITOR':
        self.affected_objects = context.selected_editable_objects
    elif context.space_data.type == 'NLA_EDITOR':
        self.report({'ERROR'}, "Retiming the NLA is unimplemented")
        return {'CANCELLED'}
    else:
        return {'CANCELLED'}

    if len(self.affected_objects) == 0:
        self.report({'ERROR'}, "No objects selected.")
        return {'CANCELLED'}

    auto_snap = get_anim_snap_mode(context)

    if auto_snap == 'SECOND':
        self.report({'ERROR'}, "Retiming does not work in \"Nearest Second\" mode.")
        return {'CANCELLED'}

    if auto_snap == 'MARKER':
        self.report({'ERROR'}, "Retiming does not work in \"Nearest Marker\" mode.")
        return {'CANCELLED'}

    if auto_snap == 'TIME_STEP':
        self.report({'ERROR'}, "Retiming does not work in \"Second Step\" mode.")
        return {'CANCELLED'}

    # Populate the list of selected animated objects
    self.retimed_object_indices = []
    self.retimed_gpencil_object_indices = []
    self.retimed_shapekey_object_indices = []

    for so_i in range(len(self.affected_objects)):
        if (self.affected_objects[so_i].animation_data != None and
                self.affected_objects[so_i].animation_data.action != None and
                self.affected_objects[so_i].animation_data.action.fcurves != None and
                len(self.affected_objects[so_i].animation_data.action.fcurves) != 0):
            self.retimed_object_indices.append(so_i)

        if self.affected_objects[so_i].type == 'GPENCIL':
            self.retimed_gpencil_object_indices.append(so_i)
        elif self.affected_objects[so_i].type == 'MESH':
            if (self.affected_objects[so_i].data != None and
                    self.affected_objects[so_i].data.shape_keys != None and
                    self.affected_objects[so_i].data.shape_keys.animation_data != None and
                    self.affected_objects[so_i].data.shape_keys.animation_data.action != None and
                    self.affected_objects[so_i].data.shape_keys.animation_data.action.fcurves != None and
                    len(self.affected_objects[so_i].data.shape_keys.animation_data.action.fcurves) != 0):
                self.retimed_shapekey_object_indices.append(so_i)


    if context.scene.retime_filter.retime_objects:
        for ro_i in range(len(self.retimed_object_indices)):
            so_i = self.retimed_object_indices[ro_i]
            for fcurve in self.affected_objects[so_i].animation_data.action.fcurves:
                fcurve.update()

    if context.scene.retime_filter.retime_shape_keys:
        for ro_i in range(len(self.retimed_shapekey_object_indices)):
            so_i = self.retimed_shapekey_object_indices[ro_i]
            for fcurve in self.affected_objects[so_i].data.shape_keys.animation_data.action.fcurves:
                fcurve.update()

    self.mouse_prev_x = 0
    self.residual_offset = 0

    self.is_keyboard_input = False
    self.keyboard_cursor_position = 0
    self.is_ctrl_down = False
    self.is_backspace_down = False
    self.is_delete_down = False
    self.is_right_arrow_down = False
    self.is_left_arrow_down = False
    self.overwrote_keyframes = False
    self.gpencil_keyframes_selected = False

    if context.scene.retime_filter.retime_objects:
        self.retimed_fcurve_indices = [[] for i in range(len(self.retimed_object_indices))]
        self.retimed_keyframes_start_indices = [[] for i in range(len(self.retimed_object_indices))]
        self.retimed_keyframes_end_indices = [[] for i in range(len(self.retimed_object_indices))]
        self.retimed_keyframes = [[] for i in range(len(self.retimed_object_indices))]
        self.post_retimed_fcurve_indices = [[] for i in range(len(self.retimed_object_indices))]
        self.post_retimed_keyframes_start_indices = [[] for i in range(len(self.retimed_object_indices))]

    if context.scene.retime_filter.retime_shape_keys:
        self.retimed_shapekey_fcurve_indices = [[] for i in range(len(self.retimed_shapekey_object_indices))]
        self.retimed_shapekey_keyframes_start_indices = [[] for i in range(len(self.retimed_shapekey_object_indices))]
        self.retimed_shapekey_keyframes_end_indices = [[] for i in range(len(self.retimed_shapekey_object_indices))]
        self.retimed_shapekey_keyframes = [[] for i in range(len(self.retimed_shapekey_object_indices))]
        self.post_retimed_shapekey_fcurve_indices = [[] for i in range(len(self.retimed_shapekey_object_indices))]
        self.post_retimed_shapekey_keyframes_start_indices = [[] for i in range(len(self.retimed_shapekey_object_indices))]

    if context.scene.retime_filter.retime_grease_pencils:
        self.retimed_gpencil_layer_indices = [[] for i in range(len(self.retimed_gpencil_object_indices))]
        self.retimed_gpencil_keyframes_start_indices = [[] for i in range(len(self.retimed_gpencil_object_indices))]
        self.retimed_gpencil_keyframes_end_indices = [[] for i in range(len(self.retimed_gpencil_object_indices))]
        self.retimed_gpencil_keyframes = [[] for i in range(len(self.retimed_gpencil_object_indices))]
        self.post_retimed_gpencil_layer_indices = [[] for i in range(len(self.retimed_gpencil_object_indices))]
        self.post_retimed_gpencil_keyframes_start_indices = [[] for i in range(len(self.retimed_gpencil_object_indices))]

    # Markers
    self.sorted_marker_frames_and_indices = []
    for m_i in range(len(context.scene.timeline_markers)):
        self.sorted_marker_frames_and_indices.append(RetimedMarkerFrameAndIndex(frame=float(context.scene.timeline_markers[m_i].frame), index=m_i))
    self.sorted_marker_frames_and_indices.sort(key=lambda x: context.scene.timeline_markers[x.index].frame)

    # Pose Markers
    self.sorted_pose_marker_frames_and_indices = []
    rowspm_i = 0
    for ro_i in range(len(self.retimed_object_indices)):
        so_i = self.retimed_object_indices[ro_i]
        pose_markers = self.affected_objects[so_i].animation_data.action.pose_markers
        if len(pose_markers) == 0:
            continue
        self.sorted_pose_marker_frames_and_indices.append(RetimedPoseMarkers(so_i))

        for pm_i in range(len(pose_markers)):
            self.sorted_pose_marker_frames_and_indices[rowspm_i].data.append(RetimedMarkerFrameAndIndex(frame=float(pose_markers[pm_i].frame), index=pm_i))
        self.sorted_pose_marker_frames_and_indices[rowspm_i].data.sort(key=lambda x: pose_markers[x.index].frame)
        rowspm_i += 1

    min_frame = GLOBAL_MAX_FRAME
    max_frame = GLOBAL_MIN_FRAME

    filtered_retiming_max_filtered_out_frame_within_duration = GLOBAL_MIN_FRAME
    filtered_retiming_max_filtered_in_frame_within_duration = GLOBAL_MIN_FRAME

    if use_markers == False:
        # Determine the min/max range of affected frames
        # NOTE: There was one rig that this didn't work on, but I think it was due to broken links
        found_selected_keyframes = False

        for ro_i in range(len(self.retimed_object_indices)):
            so_i = self.retimed_object_indices[ro_i]
            nla_tweak_strip_time_to_scene = self.affected_objects[so_i].animation_data.nla_tweak_strip_time_to_scene
            for f_i in range(len(self.affected_objects[so_i].animation_data.action.fcurves)):
                fcurve = self.affected_objects[so_i].animation_data.action.fcurves[f_i]
                if fcurve.is_valid == False or fcurve.is_empty == True:
                    continue
                for k_i in range(len(fcurve.keyframe_points)):
                    nearest_keyframe_frame = round(nla_tweak_strip_time_to_scene(fcurve.keyframe_points[k_i].co.x))
                    if fcurve.keyframe_points[k_i].select_control_point:
                        if context.scene.retime_filter.retime_objects:
                            found_selected_keyframes = True
                            if nearest_keyframe_frame < min_frame:
                                min_frame = nearest_keyframe_frame
                            if nearest_keyframe_frame > max_frame:
                                max_frame = nearest_keyframe_frame
                        else:
                            fcurve.keyframe_points[k_i].select_control_point = False
                            fcurve.keyframe_points[k_i].select_right_handle = False
                            fcurve.keyframe_points[k_i].select_left_handle = False

                    if nearest_keyframe_frame <= context.scene.frame_end:
                        if context.scene.retime_filter.retime_objects:
                            if nearest_keyframe_frame > filtered_retiming_max_filtered_in_frame_within_duration:
                                filtered_retiming_max_filtered_in_frame_within_duration = nearest_keyframe_frame
                        else:
                            if nearest_keyframe_frame > filtered_retiming_max_filtered_out_frame_within_duration:
                                filtered_retiming_max_filtered_out_frame_within_duration = nearest_keyframe_frame


        for rsko_i in range(len(self.retimed_shapekey_object_indices)):
            ssko_i = self.retimed_shapekey_object_indices[rsko_i]
            nla_tweak_strip_time_to_scene = self.affected_objects[ssko_i].data.shape_keys.animation_data.nla_tweak_strip_time_to_scene
            for skf_i in range(len(self.affected_objects[ssko_i].data.shape_keys.animation_data.action.fcurves)):
                fcurve = self.affected_objects[ssko_i].data.shape_keys.animation_data.action.fcurves[skf_i]
                if fcurve.is_valid == False or fcurve.is_empty == True:
                    continue
                for skk_i in range(len(fcurve.keyframe_points)):
                    nearest_keyframe_frame = round(nla_tweak_strip_time_to_scene(fcurve.keyframe_points[skk_i].co.x))
                    if fcurve.keyframe_points[skk_i].select_control_point:
                        if context.scene.retime_filter.retime_shape_keys:
                            found_selected_keyframes = True
                            if nearest_keyframe_frame < min_frame:
                                min_frame = nearest_keyframe_frame
                            if nearest_keyframe_frame > max_frame:
                                max_frame = nearest_keyframe_frame
                        else:
                            fcurve.keyframe_points[skk_i].select_control_point = False
                            fcurve.keyframe_points[skk_i].select_right_handle = False
                            fcurve.keyframe_points[skk_i].select_left_handle = False
                    if nearest_keyframe_frame <= context.scene.frame_end:
                        if context.scene.retime_filter.retime_shape_keys:
                            if nearest_keyframe_frame > filtered_retiming_max_filtered_in_frame_within_duration:
                                filtered_retiming_max_filtered_in_frame_within_duration = nearest_keyframe_frame
                        else:
                            if nearest_keyframe_frame > filtered_retiming_max_filtered_out_frame_within_duration:
                                filtered_retiming_max_filtered_out_frame_within_duration = nearest_keyframe_frame

        for rgo_i in range(len(self.retimed_gpencil_object_indices)):
            sgo_i = self.retimed_gpencil_object_indices[rgo_i]
            for gl_i in range(len(self.affected_objects[sgo_i].data.layers)):
                layer = self.affected_objects[sgo_i].data.layers[gl_i]
                for gk_i in range(len(layer.frames)):
                    nearest_frame = layer.frames[gk_i].frame_number
                    if layer.frames[gk_i].select:
                        if context.scene.retime_filter.retime_grease_pencils:
                            found_selected_keyframes = True
                            if nearest_frame < min_frame:
                                min_frame = nearest_frame
                            if nearest_frame > max_frame:
                                max_frame = nearest_frame
                        else:
                            layer.frames[gk_i].select = False

        if found_selected_keyframes == False:
            self.report({'ERROR'}, "Please select a range of keyframes.")
            return {'CANCELLED'}
        if max_frame <= min_frame:
            self.report({'ERROR'}, "Please select a range of keyframes.")
            return {'CANCELLED'}

    else:
        if context.space_data.type == 'DOPESHEET_EDITOR' and context.space_data.mode == 'ACTION' and context.space_data.show_pose_markers:
            found_selected_pose_markers = False
            for pose_markers in self.sorted_pose_marker_frames_and_indices:
                for pose_marker in pose_markers.data:
                    so_i = self.retimed_object_indices[pose_markers.obj_index]
                    if self.affected_objects[so_i].animation_data.action.pose_markers[pose_marker.index].select:
                        found_selected_pose_markers = True
                        if pose_marker.frame < min_frame:
                            min_frame = pose_marker.frame
                        if pose_marker.frame > max_frame:
                            max_frame = pose_marker.frame
            #print("found selected pose markers: {}".format(found_selected_pose_markers))
            if found_selected_pose_markers == False:
                self.report({'ERROR'}, "Please select a range of pose markers.")
                return {'CANCELLED'}
            if max_frame <= min_frame:
                self.report({'ERROR'}, "Please select a range of pose markers.")
                return {'CANCELLED'}
        else:
            found_selected_markers = False
            for sm_i in range(len(self.sorted_marker_frames_and_indices)):
                m_i = self.sorted_marker_frames_and_indices[sm_i].index
                if context.scene.timeline_markers[m_i].select:
                    found_selected_markers = True
                    marker_frame = self.sorted_marker_frames_and_indices[sm_i].frame
                    if marker_frame < min_frame:
                        min_frame = marker_frame
                    if marker_frame > max_frame:
                        max_frame = marker_frame

            print("found selected markers: {}".format(found_selected_markers))
            if found_selected_markers == False:
                self.report({'ERROR'}, "Please select a range of time markers.")
                return {'CANCELLED'}
            if max_frame <= min_frame:
                self.report({'ERROR'}, "Please select a range of time markers.")
                return {'CANCELLED'}

    print("frame range: {} -> {}".format(min_frame, max_frame))

    # Determine the min/max range of affected keyframes per fcurve, and select them
    # NOTE: The retimed keyframes must be selected for blender's snap operators to work.
    # This means we have to touch them up front of the apply_retiming operator.
    #
    # To minimize unnecessary checks during apply_retiming, we add the selected keyframes to the retimed_keyframes list.
    # It would be great if we could just modify them in-place, but the indices seem questionable, so iterating ranges
    # isn't going to work afaikt.
    if context.scene.retime_filter.retime_objects:
        for ro_i in range(len(self.retimed_object_indices)):
            so_i = self.retimed_object_indices[ro_i]
            nla_tweak_strip_time_to_scene = self.affected_objects[so_i].animation_data.nla_tweak_strip_time_to_scene
            # NOTE: Could use len() instead of incrementing these indices
            # Small beans though
            rf_i = 0
            # prf_i = 0
            for f_i in range(len(self.affected_objects[so_i].animation_data.action.fcurves)):
                found_first_selected_frame_in_this_fcurve = False
                found_first_post_selected_frame_in_this_fcurve = False
                fcurve = self.affected_objects[so_i].animation_data.action.fcurves[f_i]
                if fcurve.lock:
                    continue
                for k_i in range(len(fcurve.keyframe_points)):
                    keyframe_co_x = nla_tweak_strip_time_to_scene(fcurve.keyframe_points[k_i].co.x)

                    if keyframe_co_x >= min_frame and keyframe_co_x <= max_frame:
                        fcurve.keyframe_points[k_i].select_control_point = True

                        if found_first_selected_frame_in_this_fcurve == False:
                            found_first_selected_frame_in_this_fcurve = True
                            self.retimed_fcurve_indices[ro_i].append(f_i)
                            self.retimed_keyframes[ro_i].append([])
                            self.retimed_keyframes_start_indices[ro_i].append(k_i)
                            self.retimed_keyframes_end_indices[ro_i].append(k_i)
                        else:
                            self.retimed_keyframes_end_indices[ro_i][rf_i] = k_i

                        self.retimed_keyframes[ro_i][rf_i].append(RetimedKeyframeCoordinateAndHandles(
                            keyframe_co_x,
                            [
                                nla_tweak_strip_time_to_scene(fcurve.keyframe_points[k_i].handle_left[0]),
                                fcurve.keyframe_points[k_i].handle_left[1]
                            ],
                            [
                                nla_tweak_strip_time_to_scene(fcurve.keyframe_points[k_i].handle_right[0]),
                                fcurve.keyframe_points[k_i].handle_right[1]
                            ]
                        ))

                    if keyframe_co_x > max_frame and found_first_post_selected_frame_in_this_fcurve == False:
                        found_first_post_selected_frame_in_this_fcurve = True
                        self.post_retimed_fcurve_indices[ro_i].append(f_i)
                        self.post_retimed_keyframes_start_indices[ro_i].append(k_i)

                if found_first_selected_frame_in_this_fcurve:
                    rf_i += 1

    if context.scene.retime_filter.retime_shape_keys:
        for rsko_i in range(len(self.retimed_shapekey_object_indices)):
            ssko_i = self.retimed_shapekey_object_indices[rsko_i]
            nla_tweak_strip_time_to_scene = self.affected_objects[ssko_i].data.shape_keys.animation_data.nla_tweak_strip_time_to_scene
            # NOTE: Could use len() instead of incrementing these indices
            # Small beans though
            rskf_i = 0
            # prskf_i = 0
            for skf_i in range(len(self.affected_objects[ssko_i].data.shape_keys.animation_data.action.fcurves)):
                found_first_selected_frame_in_this_fcurve = False
                found_first_post_selected_frame_in_this_fcurve = False
                fcurve = self.affected_objects[ssko_i].data.shape_keys.animation_data.action.fcurves[skf_i]
                if fcurve.lock:
                    continue
                for skk_i in range(len(fcurve.keyframe_points)):
                    keyframe_co_x = nla_tweak_strip_time_to_scene(fcurve.keyframe_points[skk_i].co.x)

                    if keyframe_co_x >= min_frame and keyframe_co_x <= max_frame:
                        fcurve.keyframe_points[skk_i].select_control_point = True

                        if found_first_selected_frame_in_this_fcurve == False:
                            found_first_selected_frame_in_this_fcurve = True
                            self.retimed_shapekey_fcurve_indices[rsko_i].append(skf_i)
                            self.retimed_shapekey_keyframes[rsko_i].append([])
                            self.retimed_shapekey_keyframes_start_indices[rsko_i].append(skk_i)
                            self.retimed_shapekey_keyframes_end_indices[rsko_i].append(skk_i)
                        else:
                            self.retimed_shapekey_keyframes_end_indices[rsko_i][rskf_i] = skk_i

                        self.retimed_shapekey_keyframes[rsko_i][rskf_i].append(RetimedKeyframeCoordinateAndHandles(
                            keyframe_co_x,
                            [
                                nla_tweak_strip_time_to_scene(fcurve.keyframe_points[skk_i].handle_left[0]),
                                fcurve.keyframe_points[skk_i].handle_left[1]
                            ],
                            [
                                nla_tweak_strip_time_to_scene(fcurve.keyframe_points[skk_i].handle_right[0]),
                                fcurve.keyframe_points[skk_i].handle_right[1]
                            ]
                        ))

                    if keyframe_co_x > max_frame and found_first_post_selected_frame_in_this_fcurve == False:
                        found_first_post_selected_frame_in_this_fcurve = True
                        self.post_retimed_shapekey_fcurve_indices[rsko_i].append(skf_i)
                        self.post_retimed_shapekey_keyframes_start_indices[rsko_i].append(skk_i)

                if found_first_selected_frame_in_this_fcurve:
                    rskf_i += 1

    if context.scene.retime_filter.retime_grease_pencils:
        for rgo_i in range(len(self.retimed_gpencil_object_indices)):
            sgo_i = self.retimed_gpencil_object_indices[rgo_i]
            rgl_i = 0
            for gl_i in range(len(self.affected_objects[sgo_i].data.layers)):
                found_first_selected_frame_in_this_layer = False
                found_first_post_selected_frame_in_this_layer = False
                layer = self.affected_objects[sgo_i].data.layers[gl_i]
                if layer.lock:
                    continue
                for gk_i in range(len(layer.frames)):
                    frame_number = layer.frames[gk_i].frame_number

                    if frame_number >= min_frame and frame_number <= max_frame:
                        layer.frames[gk_i].select = True

                        if found_first_selected_frame_in_this_layer == False:
                            found_first_selected_frame_in_this_layer = True
                            self.retimed_gpencil_layer_indices[rgo_i].append(gl_i)
                            self.retimed_gpencil_keyframes[rgo_i].append([])
                            self.retimed_gpencil_keyframes_start_indices[rgo_i].append(gk_i)
                            self.retimed_gpencil_keyframes_end_indices[rgo_i].append(gk_i)
                            self.gpencil_keyframes_selected = True
                        else:
                            self.retimed_gpencil_keyframes_end_indices[rgo_i][rgl_i] = gk_i

                        self.retimed_gpencil_keyframes[rgo_i][rgl_i].append(frame_number)

                    if frame_number > max_frame and found_first_post_selected_frame_in_this_layer == False:
                        found_first_post_selected_frame_in_this_layer = True
                        self.post_retimed_gpencil_layer_indices[rgo_i].append(gl_i)
                        self.post_retimed_gpencil_keyframes_start_indices[rgo_i].append(gk_i)

                if found_first_selected_frame_in_this_layer:
                    rgl_i += 1

    self.mouse_prev_x = event.mouse_region_x
    self.start_frame = min_frame
    self.initial_timing = int(max_frame - min_frame)
    self.current_timing = self.initial_timing
    self.desired_timing = self.initial_timing
    context.window_manager.modal_handler_add(self)
    self.draw_handle = None

    if context.space_data.type == 'DOPESHEET_EDITOR':
        color = context.preferences.themes[0].dopesheet_editor.keyframe_selected
        bar_color = context.preferences.themes[0].dopesheet_editor.time_marker_line_selected
        args = (self, context, color,  bar_color)
        self.draw_handle = bpy.types.SpaceDopeSheetEditor.draw_handler_add(draw_callback_px, args, 'WINDOW', 'POST_PIXEL')
    elif context.space_data.type == 'GRAPH_EDITOR':
        color = context.preferences.themes[0].graph_editor.vertex_select
        bar_color = context.preferences.themes[0].graph_editor.time_marker_line_selected
        args = (self, context, color,  bar_color)
        self.draw_handle = bpy.types.SpaceGraphEditor.draw_handler_add(draw_callback_px, args, 'WINDOW', 'POST_PIXEL')

    return {'RUNNING_MODAL'}


def cleanup(context):
    context.workspace.status_text_set(text=None)


class RetimeKeyframes(Operator):
    """Retime range of selected items"""
    bl_idname = "scene.retime_keyframes"
    bl_label = "Retime Keyframes"
    bl_options = {'REGISTER', 'UNDO', 'GRAB_CURSOR', 'BLOCKING'}

    def modal(self, context, event):
        ret = modal_common(self, context, event, "Keyframes")
        if (ret != {'RUNNING_MODAL'}):
            cleanup(context)
        return ret

    def invoke(self, context, event):
        return invoke_common(self, context, event, use_markers=False)


def retime_keyframes_menu(self, context):
    self.layout.operator("scene.retime_keyframes")


class RetimeMarkers(Operator):
    """Retime range of selected items"""
    bl_idname = "scene.retime_markers"
    bl_label = "Retime Markers"
    bl_options = {'REGISTER', 'UNDO', 'GRAB_CURSOR', 'BLOCKING'}

    def modal(self, context, event):
        ret = modal_common(self, context, event, "Markers")
        if (ret != {'RUNNING_MODAL'}):
            cleanup(context)
        return ret

    def invoke(self, context, event):
        return invoke_common(self, context, event, use_markers=True)


def retime_markers_menu(self, context):
    self.layout.operator("scene.retime_markers")


class RetimeFilter(PropertyGroup):
    retime_objects: BoolProperty(
        name = "Objects",
        description = "Toggle retiming of object keyframes",
        default = True
    )

    retime_grease_pencils: BoolProperty(
        name = "Grease Pencil Strokes",
        description = "Toggle retiming of grease pencil keyframes",
        default = True
    )

    retime_shape_keys: BoolProperty(
        name = "Shape Keys",
        description = "Toggle retiming of shape key keyframes",
        default = True
    )

def retime_filter_panel_draw(self, context):
    layout = self.layout

    row = layout.row()
    if context.scene.retime_filter.retime_grease_pencils == False and context.scene.retime_filter.retime_shape_keys == False:
        row.label(text="Objects", icon='CHECKBOX_HLT')
    else:
        row.prop(context.scene.retime_filter, "retime_objects")

    row = layout.row()
    if context.scene.retime_filter.retime_objects == False and context.scene.retime_filter.retime_shape_keys == False:
        row.label(text="Grease Pencil Strokes", icon='CHECKBOX_HLT')
    else:
        row.prop(context.scene.retime_filter, "retime_grease_pencils")

    row = layout.row()
    if context.scene.retime_filter.retime_grease_pencils == False and context.scene.retime_filter.retime_objects == False:
        row.label(text="Shape Keys", icon='CHECKBOX_HLT')
    else:
        row.prop(context.scene.retime_filter, "retime_shape_keys")

class RetimeDopeSheetFilterPanel(Panel):
    bl_label = "Retiming Filter"
    bl_idname = "DOPESHEET_PT_retiming_filter"
    bl_space_type = 'DOPESHEET_EDITOR'
    bl_region_type = 'UI'
    bl_context = "scene"
    bl_category = "Retiming"

    def draw(self, context):
        retime_filter_panel_draw(self, context)


class RetimeGraphFilterPanel(Panel):
    bl_label = "Retiming Filter"
    bl_idname = "GRAPH_PT_retiming_filter"
    bl_space_type = 'GRAPH_EDITOR'
    bl_region_type = 'UI'
    bl_context = "scene"
    bl_category = "Retiming"

    def draw(self, context):
        retime_filter_panel_draw(self, context)

def retime_preferences_panel_draw(self, context):
    layout = self.layout

    row = layout.row()
    row.prop(context.preferences.addons[__package__].preferences, "adjust_playback_duration")
    row = layout.row()
    row.prop(context.preferences.addons[__package__].preferences, "adjust_playhead_position")

class RetimeDopeSheetPreferencesPanel(Panel):
    bl_label = "Retiming Preferences"
    bl_idname = "DOPESHEET_PT_retiming_preferences"
    bl_space_type = 'DOPESHEET_EDITOR'
    bl_region_type = 'UI'
    bl_context = "scene"
    bl_category = "Retiming"

    def draw(self, context):
        retime_preferences_panel_draw(self, context)


class RetimeGraphPreferencesPanel(Panel):
    bl_label = "Retiming Preferences"
    bl_idname = "GRAPH_PT_retiming_preferences"
    bl_space_type = 'GRAPH_EDITOR'
    bl_region_type = 'UI'
    bl_context = "scene"
    bl_category = "Retiming"

    def draw(self, context):
        retime_preferences_panel_draw(self, context)
